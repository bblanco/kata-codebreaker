package es.bblanco.kata.codebreaker;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;

public class MalignoCaptureTest {

	private CodeChecker codeChecker;
	private Screen screen;
	private MalignoCapture malignoCapture;

	@Before
	public void setUp() {
		codeChecker = mock(CodeChecker.class);
		screen = mock(Screen.class);
		malignoCapture = new MalignoCapture(codeChecker, screen);
	}

	@Test
	public void startGame_GenerateRandomSecurityCode() {
		malignoCapture.startGame();
		verify(codeChecker).generateRandomSecurityCode();
	}

	@Test
	public void onEnterCode_CheckCode() throws NotValidCodeException {
		malignoCapture.onEnterCode("AMYN");
		verify(codeChecker).checkCode("AMYN");
	}

	@Test
	public void onEnterCode_ShowResult() throws NotValidCodeException {
		Result result = mock(Result.class);
		when(codeChecker.checkCode("AMYN")).thenReturn(result);
		malignoCapture.onEnterCode("AMYN");
		verify(screen).showResult(result);
	}

	@Test
	public void onEnterInvalidCode_NoShowResult() throws NotValidCodeException {
		Result result = mock(Result.class);
		when(codeChecker.checkCode("AMYN")).thenThrow(
				new NotValidCodeException("Invalid code"));
		try {
			malignoCapture.onEnterCode("AMYN");
		} catch (NotValidCodeException e) {
			// ignore is expect
		}

		verify(screen, never()).showResult(result);
	}

}
