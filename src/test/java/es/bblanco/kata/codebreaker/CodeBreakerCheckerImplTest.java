package es.bblanco.kata.codebreaker;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class CodeBreakerCheckerImplTest {
	
	private static final int LENGTH_SECURITY_CODE = 4;
	private CodeBreakerCheckerImpl codeBreakerChecker;
	String  validCharacters = "RAMVIN";

	@Before
	public void setUp() throws Exception {
		codeBreakerChecker = new CodeBreakerCheckerImpl();
	}
	
	@Test
	public void generateRandomSecurityCode_securityCodeIsNotNull() {
		codeBreakerChecker.generateRandomSecurityCode();
		assertNotNull(codeBreakerChecker.getSecurityCode());
	}
	
	@Test
	public void generateRandomSecurityCode_validCharacteres(){
		codeBreakerChecker.generateRandomSecurityCode();
		String securityCode = codeBreakerChecker.getSecurityCode();
		
		for (int i = 0; i < securityCode.length(); i++) {
			String character = securityCode.substring(i, i+1);
			if (validCharacters.indexOf(character) == -1){
				fail("the character " +  character + " is not valid");
			}
		}
	}
	
	@Test
	public void generateRandomSecurityCode_isCorrectLength() {
		codeBreakerChecker.generateRandomSecurityCode();
		assertEquals(LENGTH_SECURITY_CODE, codeBreakerChecker.getSecurityCode().length());
	}
	
	@Test
	public void generateRandomSecurityCode(){
		codeBreakerChecker.generateRandomSecurityCode();
		String securityCode1 = codeBreakerChecker.getSecurityCode();
		codeBreakerChecker.generateRandomSecurityCode();
		String securityCode2 = codeBreakerChecker.getSecurityCode();
		assertNotSame(securityCode1, securityCode2);
	}
	
	@Test(expected=NotValidCodeException.class)
	public void setSecurityCode_invalidLength() throws NotValidCodeException{
		codeBreakerChecker.setSecurityCode("AAAAAAAA");
	}
	
	@Test(expected=NotValidCodeException.class)
	public void setSecurityCode_invalidCharacter() throws NotValidCodeException{
		codeBreakerChecker.setSecurityCode("FFFF");
	}
	
	@Test(expected=NotValidCodeException.class)
	public void setSecurityCode_null() throws NotValidCodeException{
		codeBreakerChecker.setSecurityCode(null);
	}
	
	@Test
	public void checkCode_XX() throws NotValidCodeException{
		codeBreakerChecker.setSecurityCode("RANI");
		Result result = codeBreakerChecker.checkCode("RMVI");
		assertEquals("XX", result.getClue());
	}
	
	@Test
	public void checkCode_asterisc() throws NotValidCodeException{
		codeBreakerChecker.setSecurityCode("RANI");
		Result result = codeBreakerChecker.checkCode("ARVV");
		assertEquals("**", result.getClue());
	}
	
	@Test
	public void checkCode_mix1() throws NotValidCodeException{
		codeBreakerChecker.setSecurityCode("NRRI");
		Result result = codeBreakerChecker.checkCode("RRRN");
		assertEquals("XX*", result.getClue());
	}
	
	@Test
	public void checkCode_mix2() throws NotValidCodeException{
		codeBreakerChecker.setSecurityCode("NRRI");
		Result result = codeBreakerChecker.checkCode("RRVN");
		assertEquals("X**", result.getClue());
	}
	
	@Test
	public void checkCode_mix3() throws NotValidCodeException{
		codeBreakerChecker.setSecurityCode("RANI");
		Result result = codeBreakerChecker.checkCode("VNVI");
		assertEquals("X*", result.getClue());
	}
	
}
