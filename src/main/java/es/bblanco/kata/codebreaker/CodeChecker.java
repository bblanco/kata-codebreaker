package es.bblanco.kata.codebreaker;

public interface CodeChecker {

	Result checkCode(String string) throws NotValidCodeException;

	void generateRandomSecurityCode();

}
