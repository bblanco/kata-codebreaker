package es.bblanco.kata.codebreaker;

import java.util.Random;

public class CodeBreakerCheckerImpl implements CodeChecker {
	private static final String[] VALID_CHARACTERS = new String[] { "R", "A",
			"M", "V", "I", "N" };
	private static final int LENGTH_CODE = 4;
	private static final String CLUE_SUCCESS= "XXXX";

	private String securityCode;

	public Result checkCode(String code) throws NotValidCodeException {
		
		Result result = new Result();
		result.setSuccess(false);
		validateCode(code);
		
		if (securityCode.equals(code)){
			result.setSuccess(true);
			result.setClue(CLUE_SUCCESS);
			return result;
		}
		
		String clue = "";
		String codeAux = new String(code);
		String securityCodeAux = new String(securityCode);
		
		for (int i = 0; i < codeAux.length(); i++) {
			String character = codeAux.substring(i, i + 1);
			if (securityCodeAux.substring(i, i + 1).equals(character)){
				clue = clue + "X";
				securityCodeAux = removeCharAt(securityCodeAux,i);
				codeAux =  removeCharAt(codeAux,i);
				i--;
			}
		}
		
		for (int i = 0; i < codeAux.length(); i++) {
			String character = codeAux.substring(i, i + 1);
			int indexLocation = securityCodeAux.indexOf(character);
			if (indexLocation != -1){
				clue = clue + "*";
				securityCodeAux = removeCharAt(securityCodeAux,indexLocation);
				codeAux =  removeCharAt(codeAux,i);
				i--;
			}	
		}
		
		result.setClue(clue);
		return result;
	}
	

	private String removeCharAt(String s, int pos) {
		StringBuffer buf = new StringBuffer(s.length() - 1);
		buf.append(s.substring(0, pos)).append(s.substring(pos + 1));
		return buf.toString();
	}

	public void generateRandomSecurityCode() {
		securityCode = "";
		Random randomGenerator = new Random();
		for (int i = 0; i < LENGTH_CODE; i++) {
			int random = randomGenerator.nextInt(LENGTH_CODE);
			securityCode = securityCode + VALID_CHARACTERS[random];
		}

	}

	public String getSecurityCode() {
		return securityCode;
	}

	public void setSecurityCode(String securityCode) throws NotValidCodeException {
		validateCode(securityCode);
		this.securityCode = securityCode;
	}

	private void validateCode(String code) throws NotValidCodeException {
		if (code == null){
			throw new NotValidCodeException("Code must be not null");
		} else if (code.length() != LENGTH_CODE){
			throw new NotValidCodeException("Code length must be " + LENGTH_CODE);
		} else {
			String validCharacters = join(VALID_CHARACTERS);
			for (int i = 0; i < code.length(); i++) {
				String character = code.substring(i, i+1);
				if (validCharacters.indexOf(character) == -1){
					throw new NotValidCodeException("["+ character + "] is a not valid character");
				}
			}
		}
	}
	
	private String join(String[] input)
	{
	    StringBuilder sb = new StringBuilder();
	    for(String value : input)
	    {
	        sb.append(value);
	    }
	    return sb.toString();
	}

}
