package es.bblanco.kata.codebreaker;

public interface Screen {

	void showResult(Result result);

}
