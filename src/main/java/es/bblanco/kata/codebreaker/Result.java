package es.bblanco.kata.codebreaker;

public class Result {
	private boolean success;
	private String  clue;
	
	
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getClue() {
		return clue;
	}
	public void setClue(String clue) {
		this.clue = clue;
	}
	
	
}
