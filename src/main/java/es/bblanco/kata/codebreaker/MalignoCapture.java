package es.bblanco.kata.codebreaker;

import java.util.Scanner;

public class MalignoCapture {
	
	private CodeChecker codeChecker;
	private Screen screen;

	public MalignoCapture(CodeChecker codeChecker, Screen screen) {
		super();
		this.codeChecker = codeChecker;
		this.screen = screen;
	}

	public Result onEnterCode(String code) throws NotValidCodeException {
		Result result = codeChecker.checkCode(code);
		screen.showResult(result);
		return result;
	}

	public void startGame() {
		codeChecker.generateRandomSecurityCode();
	}
	
	
	
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Iniciado CodeBreaker");
		System.out.println("====================");
		System.out.println("Caracteres posibles [R] [A] [M] [V] [I] [N]");
		
		System.out.println("");
		
		CodeChecker codeChecker = new CodeBreakerCheckerImpl();
		Screen screen = new ScreenConsoleImpl();
		MalignoCapture capture = new MalignoCapture(codeChecker, screen);
		capture.startGame();
		boolean success = false;
		while(!success){
			String attempCode = scanner.nextLine();
			Result result;
			try {
				result = capture.onEnterCode(attempCode);
				success = result.isSuccess();
			} catch (NotValidCodeException e) {
				System.out.println(e.getMessage());
			}
			System.out.println("");
			
		}
	}

}
