package es.bblanco.kata.codebreaker;

public class NotValidCodeException extends Exception {

	private static final long serialVersionUID = 1L;

	public NotValidCodeException(String message) {
		super(message);
	}

	
}
