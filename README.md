# Desafio enero 12 meses 12 katas
Kata CodeBreaker

Consigue descifrar el código de seguridad para acceder al super laboratorio científico del doctor Maligno. No te será fácil, pero seguro que será divertido!


Resumen de las normas

Al comenzar el juego, se genera una clave de colores.
En cada intento hay que introducir 4 letras correspondientes a los colores posibles.
Después de cada intento, el juego informará de los aciertos encontrados (según la reglas explicadas).
El juego termina cuando el código de seguridad ha sido acertado.
No hay un límite máximo de intentos.